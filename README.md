# MrFox Token

MrFox Token

## Getting started

Download file https://gitlab.com/rangsarn/mrfox-token/-/blob/main/mrfox.sol
Compile with https://remix.ethereum.org/

## Download files

```
cd existing_repo
git remote add origin https://gitlab.com/rangsarn/mrfox-token.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/rangsarn/mrfox-token/-/settings/integrations)


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Compile](https://remix.ethereum.org/)
- [ ] [Deploy to BEP20](https://remix.ethereum.org/)

***

## Name
MrFox Token.

## Description
White paper: https://www.mrfox.com/Mr-FOX-Whitpaper-v1-eng.pdf

## Support
rangsarn@gmail.com

## Roadmap
Find roadmap on website: https://www.mrfox.com/

## Authors and acknowledgment
 * Contact: Dome C. <dome@tel.co.th>
 *          Tokenine Inc
 *          256 Chapman Road STE 105-4
 *          Newark New Castle
 *          Delaware(DE) 19702

## License
SPDX-License-Identifier: MIT

## Project status
Go live.
