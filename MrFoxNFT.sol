// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721URIStorageUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721BurnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/Base64Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/structs/EnumerableSetUpgradeable.sol";

contract MrFoxNFT is Initializable,ERC721Upgradeable,ERC721URIStorageUpgradeable, ERC721BurnableUpgradeable, ERC721EnumerableUpgradeable, AccessControlUpgradeable {

    using Base64Upgradeable for string;
    using CountersUpgradeable for CountersUpgradeable.Counter;
    using EnumerableSetUpgradeable for EnumerableSetUpgradeable.UintSet;

    CountersUpgradeable.Counter internal _tokenIdCounter;

    //Detail
    string private _baseImageURI; 
    mapping(uint256 => string) internal _tokenNames;
    mapping(uint256 => string) internal _tokenDescriptions;
    mapping(uint256 => string) internal _tokenImages;
    mapping(uint256 => string) internal _tokenSomeStrings;

    mapping(uint256 => address) internal _minter;
    mapping(uint256 => uint256) internal _mintTimes;

    //Royalty
    mapping(uint256 => uint256) internal _rootOfTokens;
    mapping(uint256 => address) internal _receiverRootOfTokens;
    mapping(address => mapping (uint256 => EnumerableSetUpgradeable.UintSet)) internal _ownerTokenChilds;

    function initialize(string memory baseTokenURI,address addressAdmin) initializer public {
        __ERC721_init("Mr.Fox NFT", "Mr.FOX NFT");
        __ERC721URIStorage_init();
        __ERC721Burnable_init();
        __ERC721Enumerable_init();
        __AccessControl_init();

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(DEFAULT_ADMIN_ROLE, addressAdmin);
        setBaseURI(baseTokenURI);
        //Not have token 0
        _tokenIdCounter.increment();
    }
    
    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721Upgradeable,ERC721EnumerableUpgradeable,AccessControlUpgradeable) returns (bool){
        return super.supportsInterface(interfaceId);
    }

    // The following functions are overrides required by Solidity.    
    function _beforeTokenTransfer(address from, address to, uint256 tokenId) internal override(ERC721Upgradeable, ERC721EnumerableUpgradeable){
          if(_exists(tokenId)){
            //transferRoot
            if(_rootOfTokens[tokenId] == 0){
                if(ownerOf(tokenId) != to){
                    EnumerableSetUpgradeable.UintSet storage burnChildTokens = _ownerTokenChilds[ownerOf(tokenId)][tokenId];
                    uint256 length = burnChildTokens.length();
                    for (uint256 i = 0; i < length; ++i) {
                        _burn(burnChildTokens.at(i));
                    }
                }
            }else{
            //transferChild
                _ownerTokenChilds[ownerOf(tokenId)][rootOfToken(tokenId)].remove(tokenId);
            }
          }
          super._beforeTokenTransfer(from,to,tokenId);
    }

    function _burn(uint256 tokenId) internal virtual override(ERC721Upgradeable, ERC721URIStorageUpgradeable) {
        if (bytes(_tokenImages[tokenId]).length != 0) {
            delete _tokenImages[tokenId];
        }
        if (bytes(_tokenNames[tokenId]).length != 0) {
            delete _tokenNames[tokenId];
        }
        if (bytes(_tokenDescriptions[tokenId]).length != 0) {
            delete _tokenDescriptions[tokenId];
        }
        if (bytes(_tokenSomeStrings[tokenId]).length != 0) {
            delete _tokenSomeStrings[tokenId];
        }
        if (_minter[tokenId] != address(0)) {
            delete _minter[tokenId];
        }
        if (_mintTimes[tokenId] != 0) {
            delete _mintTimes[tokenId];
        }
        if (_receiverRootOfTokens[tokenId] != address(0)) {
            delete _receiverRootOfTokens[tokenId];
        }
        if (_rootOfTokens[tokenId] != 0) {
            delete _rootOfTokens[tokenId];
        }
        if(isRootToken(tokenId)){
            delete _ownerTokenChilds[ownerOf(tokenId)][tokenId];
        }else{
            _ownerTokenChilds[ownerOf(tokenId)][rootOfToken(tokenId)].remove(tokenId);
        }
        super._burn(tokenId);
    }

    function safeMint(address to,string memory uri,string memory name,string memory description, string memory someString) public onlyRole(DEFAULT_ADMIN_ROLE) returns (uint256) {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(to, tokenId);
        
        _tokenImages[tokenId] = uri;
        _tokenNames[tokenId] = name;
        _tokenDescriptions[tokenId] = description;
        _tokenSomeStrings[tokenId] = someString;

        _minter[tokenId] = to;
        _mintTimes[tokenId] = block.timestamp;
        _receiverRootOfTokens[tokenId] = to;

        return tokenId;
    }

    function safeMintByToken(uint256 tokenRootId,address to) public onlyRole(DEFAULT_ADMIN_ROLE) returns (uint256){
        _existsToken(tokenRootId);
        require(_isApprovedOrOwner(to, tokenRootId), "Mint caller is not owner token");
        uint256 tokenChildId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(to, tokenChildId);
        
        _tokenImages[tokenChildId] = _getTokenImage(tokenRootId);
        _tokenNames[tokenChildId] = _getTokenName(tokenRootId);
        _tokenDescriptions[tokenChildId] = _getTokenDescription(tokenRootId);
        _tokenSomeStrings[tokenChildId] = _getTokenSomeString(tokenRootId);

        _minter[tokenChildId] = to;
        _mintTimes[tokenChildId] = block.timestamp;
        _rootOfTokens[tokenChildId] = tokenRootId;
        _receiverRootOfTokens[tokenChildId] = _receiverRootOfTokens[tokenRootId];
        _ownerTokenChilds[to][tokenRootId].add(tokenChildId);
        return tokenChildId;
    }

    //Detail
    function tokenURI(uint256 tokenId) public view virtual override(ERC721Upgradeable, ERC721URIStorageUpgradeable) returns(string memory){
        _existsToken(tokenId);
       
        string memory json = Base64Upgradeable.encode(bytes(string(abi.encodePacked(
            "{\"name\": \"",_getTokenName(tokenId),"\",",
            "\"description\": \"",_getTokenDescription(tokenId),"\",",
            "\"image\": \"",_baseImageURI,_getTokenImage(tokenId),"\"",
            _getTokenSomeString(tokenId),
            "}")
            ))
        );
        return string(abi.encodePacked("data:application/json;base64,", json));
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return _baseImageURI;
    }

    function setBaseURI(string memory baseImageURI) public virtual onlyRole(DEFAULT_ADMIN_ROLE){
        _baseImageURI = baseImageURI;
    }

    function _getTokenImage(uint256 tokenId) internal view virtual returns (string memory) {
        _existsToken(tokenId);
        return _tokenImages[tokenId];
    }

    function _getTokenName(uint256 tokenId) internal view virtual returns (string memory) {
        _existsToken(tokenId);
        return _tokenNames[tokenId];
    }

    function _getTokenDescription(uint256 tokenId) internal view virtual returns (string memory) {
        _existsToken(tokenId);
        return _tokenDescriptions[tokenId];
    }

    function _getTokenSomeString(uint256 tokenId) internal view virtual returns (string memory) {
        _existsToken(tokenId);
        return _tokenSomeStrings[tokenId];
    }

    function setTokenSomeString(uint256 tokenId,string memory someString) public virtual onlyRole(DEFAULT_ADMIN_ROLE){
        _existsToken(tokenId);
        _tokenSomeStrings[tokenId] = someString;
    }

    function minterOf(uint256 tokenId) public view virtual returns (address) {
        _existsToken(tokenId);
        return _minter[tokenId];
    }

    function mintTimeOf(uint256 tokenId) public view virtual returns (uint256) {
        _existsToken(tokenId);
        return _mintTimes[tokenId];
    }

    //Royalty
    function isRootToken(uint256 tokenId) public view virtual returns (bool) {
        _existsToken(tokenId);
        return _rootOfTokens[tokenId] == 0;
    }

    function rootOfToken(uint256 tokenId) public view virtual returns (uint256) {
        _existsToken(tokenId);
        require(_rootOfTokens[tokenId]!=0, "This token is root.");
        return _rootOfTokens[tokenId];
    }

    function receiverRootOfToken(uint256 tokenId) public view virtual returns (address) {
        _existsToken(tokenId);
        return _receiverRootOfTokens[tokenId];
    }

    //validate
    function _existsToken(uint256 tokenId) internal view virtual{
        require(_exists(tokenId), "Not found token.");
    }
}