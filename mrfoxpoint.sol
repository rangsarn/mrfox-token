// SPDX-License-Identifier: MIT

/**
 * 
 * MrFOX ERC20  Smartcontract
 * Contact: Rangsarn S. <rangsarn@gmail.com>
 *          MISTER FOX CO., LTD.
 *          Phlabphla, Wangthonglang,
 *          Bangkok 10310
 *          02-9575771-75
 * 
*/

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract MrFOXERC20 is ERC20, AccessControl, Ownable {
    IERC20 public Token;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");

    constructor(address _admin) ERC20("MrFOX POINT TOKEN", "POINT") {

        _setupRole(DEFAULT_ADMIN_ROLE, _admin);
        _setupRole(MINTER_ROLE, _admin);
        _setupRole(BURNER_ROLE, _admin);

    }

    function mint(address to, uint256 amount) public onlyRole(MINTER_ROLE) {
        _mint(to, amount);
    }

    function burnfrom(address from, uint256 amount) public onlyRole(BURNER_ROLE) {
        _burn(from, amount);
    }

    function burn(uint256 amount) external {
        _burn(msg.sender, amount);
    } 
    function claimToken(IERC20 token) public  {
        require(hasRole(DEFAULT_ADMIN_ROLE, msg.sender), "Caller is not a administrator");
        token.transfer(msg.sender, token.balanceOf(address(this)));
    }

}